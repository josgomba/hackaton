package com.hackaton.hackaton;

import com.hackaton.hackaton.models.EmpleadoModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class HackatonApplication {

	public static ArrayList<EmpleadoModel> empleados = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(HackatonApplication.class, args);

		HackatonApplication.empleados = HackatonApplication.getEmpleados();
	}

	private static ArrayList<EmpleadoModel> getEmpleados() {

		empleados.add(
				new EmpleadoModel(
						"1",
						"Pepe",
						"Rodriguez",
						50,
						"Analista",
						"Factoria MX"
				)
		);

		empleados.add(
				new EmpleadoModel(
						"2",
						"Juan",
						"Cuesta",
						21,
						"Programador Junior",
						"Informatic S.A"
				)
		);

		empleados.add(
				new EmpleadoModel(
						"3",
						"Jorge",
						"Perez",
						30,
						"Programador Senior",
						"Programmer S.L"
				)
		);

		empleados.add(
				new EmpleadoModel(
						"4",
						"Belen",
						"Perez",
						41,
						"Analista Orgánico",
						"Programmer S.L"
				)
		);

		empleados.add(
				new EmpleadoModel(
						"5",
						"Carlos",
						"Torres",
						37,
						"Analista funcional",
						"Tecnology S.A."
				)
		);

		return empleados;

	}
}