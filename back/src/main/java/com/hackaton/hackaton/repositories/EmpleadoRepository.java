package com.hackaton.hackaton.repositories;

import com.hackaton.hackaton.HackatonApplication;
import com.hackaton.hackaton.models.EmpleadoModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class EmpleadoRepository {

    //Método que devuelve todos los empleados activos.
    public List<EmpleadoModel> getEmpleados() {
        System.out.println("getEmpleados en EmpleadoRepository");

        return HackatonApplication.empleados;
    }

    //Método que devuelve un empleado filtrado por ID
    public Optional<EmpleadoModel> getEmpleado(String id) {
        System.out.println("getEmpleado en EmpleadoRepository. (ID a buscar: "+id+")");

        Optional<EmpleadoModel> empleadoToFind = Optional.empty();

        for (EmpleadoModel empleadosInList : HackatonApplication.empleados ) {
            if(empleadosInList.getIdEmpleado().equals(id)) {
                empleadoToFind = Optional.of(empleadosInList);
            }
        }

        return empleadoToFind;
    }

    //Método que devuelve todos los empleados de X edad.
    public List<EmpleadoModel> getEmpleadosByAge(int edad) {
        System.out.println("getEmpleadoByAge en EmpleadoRepository. (Edad a buscar: "+edad+")");

        List<EmpleadoModel> empleadoToFind = new ArrayList<>();

        for (EmpleadoModel empleadosInList : HackatonApplication.empleados ) {
            if(empleadosInList.getEdad() == edad) {
                empleadoToFind.add(empleadosInList);
            }
        }

        return empleadoToFind;
    }

    //Método que añade un empleado al arrayList
    public EmpleadoModel addEmpleado(EmpleadoModel empleadoToCreate) {
        System.out.println("addEmpleado en EmpleadoRepository");
        HackatonApplication.empleados.add(empleadoToCreate);

        return empleadoToCreate;
    }

    //Método que actualiza un empleado
    public EmpleadoModel updateEmpleado(EmpleadoModel empleadoToModify) {
        System.out.println("updateEmpleado en EmpleadoRepository");
        System.out.println("empleado a actualizar su id es: "+empleadoToModify.getIdEmpleado());

        Optional<EmpleadoModel> empleadoToFind = getEmpleado(empleadoToModify.getIdEmpleado());

        if(empleadoToFind.isPresent()) {
            System.out.println("Empleado encontrado");

            EmpleadoModel empleadoUpdated = empleadoToFind.get();

            if(empleadoToModify.getNombre()    != null)  {empleadoUpdated.setNombre(empleadoToModify.getNombre());}
            if(empleadoToModify.getApellido()  != null)  {empleadoUpdated.setApellido(empleadoToModify.getApellido()); }
            if(empleadoToModify.getEdad()      != 0)     {empleadoUpdated.setEdad(empleadoToModify.getEdad());}
            if(empleadoToModify.getCategoria() != null)  {empleadoUpdated.setCategoria(empleadoToModify.getCategoria());}
            if(empleadoToModify.getEmpresa()   != null)  {empleadoUpdated.setEmpresa(empleadoToModify.getEmpresa());}
        }

        return empleadoToModify;
    }

    //Método que borra un empleado
    public Optional<EmpleadoModel> deleteEmpleado(EmpleadoModel empleadoToDeleted) {
        System.out.println("deleteEmpleado en EmpleadoRepository");

        Optional<EmpleadoModel> empleadoToFind = getEmpleado(empleadoToDeleted.getIdEmpleado());

        if(empleadoToFind.isPresent()) {
            HackatonApplication.empleados.remove(empleadoToDeleted);
        }

        return empleadoToFind;
    }

    //Método para obtener la última ID de empleado creada.
    public String getLastId() {

        int ultimoEmpleado = HackatonApplication.empleados.size()-1;
        String lastId = null;

        if(ultimoEmpleado<0) {
            lastId = "0";
        }else {
            lastId = HackatonApplication.empleados.get(ultimoEmpleado).getIdEmpleado();
        }

        return lastId;
    }


}
