package com.hackaton.hackaton.services;

import com.hackaton.hackaton.models.EmpleadoModel;
import com.hackaton.hackaton.repositories.EmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmpleadoService {

    @Autowired
    EmpleadoRepository empleadoRepository;

    public List<EmpleadoModel> getEmpleados() {

        System.out.println("getEmpleados in EmpleadoService");

        List<EmpleadoModel> result = empleadoRepository.getEmpleados();

        return result;
    }

    public Optional<EmpleadoModel> getEmpleado(String idEmpleado) {

        System.out.println("El id del empleado es " + idEmpleado);

        Optional<EmpleadoModel> empleadofind = empleadoRepository.getEmpleado(idEmpleado);

        return empleadofind;
    }

    public List<EmpleadoModel> getEmpleadosByAge(int edad) {

        List<EmpleadoModel> edadFind = empleadoRepository.getEmpleadosByAge(edad);

        System.out.println("La edad del empleado es " + edadFind);

        return edadFind;
    }

    public boolean addEmpleado (EmpleadoModel empleado) {

        boolean result = false;

        EmpleadoModel empleadoAdd = empleadoRepository.addEmpleado(empleado);

        if (!empleadoAdd.getIdEmpleado().equals("0") ) {
            result = true;
        }

        return result;
    }

    public boolean updateEmpleado (EmpleadoModel empleado) {

        System.out.println("updateEmpleado en empleadoService");

        boolean result = false;

        EmpleadoModel empleadoUpdate = empleadoRepository.updateEmpleado(empleado);

        if (!empleadoUpdate.getIdEmpleado().equals("0")) {
            result = true;
        }

        return result;
    }

    public boolean deleteEmpleado (String idEmpleado) {

        System.out.println("El id del empleado a borrar es :" + idEmpleado);

        Optional<EmpleadoModel> empleadodelete = empleadoRepository.getEmpleado(idEmpleado);

        boolean result = false;

        if (empleadodelete.isPresent() ) {
            Optional<EmpleadoModel> empleadoIsDeleted = empleadoRepository.deleteEmpleado(empleadodelete.get());
            if (empleadoIsDeleted.isPresent()) {
                result = true;
            }

        }

        return result;
    }

    public String getLastId() {
        System.out.println("Vamos a obtener el último ID de empleado.");

        String lastId = empleadoRepository.getLastId();

        return lastId;
    }
}