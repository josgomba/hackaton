package com.hackaton.hackaton.controllers;

import com.hackaton.hackaton.models.EmpleadoModel;
import com.hackaton.hackaton.services.EmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/hackaton/v1")
//CORS
@CrossOrigin(origins= "*", methods= {RequestMethod.GET, RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})

public class EmpleadoController {

    private final String ERROR_CREATE = "No se ha podido crear el empleado porque ";
    private final String NO_INFORMADO = "no se ha informado";
    private final String VALOR_ERRONEO = "es errónea";
    private final String EMPLEADO_CREATED = "Empleado añadido correctamente";
    private final String EMPLEADO_NO_CREATED = "No se ha podido añadir el empleado";

    @Autowired
    EmpleadoService empleadoService;

    //Obtener todos los empleados
    @GetMapping("/empleados")
    public ResponseEntity<Object> getEmpleados(@RequestParam(required = false, defaultValue = "0") int age) {

        if (age <= 0) {
            System.out.println("getEmpleados en Controller");

            return new ResponseEntity<>(this.empleadoService.getEmpleados(), HttpStatus.OK);
        } else {
            System.out.println("getEmpleadosByAge en Controller");
            System.out.println(" la edad a buscar es " + age);

            List<EmpleadoModel> result = this.empleadoService.getEmpleadosByAge(age);

            return new ResponseEntity<>(
                    result.isEmpty() ? "No se han encontrado empleados con esas edad" : result,
                    result.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK
            );
        }
    }

    //Buscar un IdEmpleado
    @GetMapping("/empleados/{id}")
    public ResponseEntity<Object> getEmpleado(@PathVariable String id) {
        System.out.println("getEmpleado en Controller");
        System.out.println("El id del empleado a buscar es " + id);

        Optional<EmpleadoModel> result = this.empleadoService.getEmpleado(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Empleado no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    //Añadir un empleado
    @PostMapping("/empleados")
    public ResponseEntity<String> addEmpleado(@RequestBody EmpleadoModel empleado) {
        System.out.println("Addempleado en controller");
        System.out.println("El id del empleado a añadir es " + empleado.getNombre());
        System.out.println("El id del empleado a añadir es " + empleado.getApellido());
        System.out.println("El id del empleado a añadir es " + empleado.getEdad());
        System.out.println("El id del empleado a añadir es " + empleado.getCategoria());
        System.out.println("El id del empleado a añadir es " + empleado.getEmpresa());

        ResponseEntity<String> resultado = validarEmpleado(empleado);

        if(resultado == null){

        String  newIdEmpleado = empleadoService.getLastId();

        Integer newIdInt = Integer.parseInt(newIdEmpleado)+1;

        newIdEmpleado = String.valueOf(newIdInt);

        empleado.setIdEmpleado(newIdEmpleado);

        boolean addEmpleado = this.empleadoService.addEmpleado(empleado);

            resultado = new ResponseEntity<>(
                    addEmpleado ? "Empleado añadido" : "No se ha podido actualizar",
                    addEmpleado ? HttpStatus.CREATED : HttpStatus.NOT_FOUND
            );
        }
       return resultado;
    }

    //Actualizar empleado
    @PutMapping("/empleados/{idEmpleado}")
    public ResponseEntity<String> updateEmpleado(@RequestBody EmpleadoModel empleado, @PathVariable String idEmpleado) {
        System.out.println("Update empleado en controller");
        System.out.println("idEmpledo por parametro" + idEmpleado);
        System.out.println("Id empleado a actualizar" + empleado.getIdEmpleado());
        System.out.println("Nombre empleado a actualizar: " + empleado.getNombre());
        System.out.println("Nombre empleado a actualizar: " + empleado.getApellido());
        System.out.println("Nombre empleado a actualizar: " + empleado.getEdad());
        System.out.println("Nombre empleado a actualizar: " + empleado.getCategoria());
        System.out.println("Nombre empleado a actualizar: " + empleado.getEmpresa());

        empleado.setIdEmpleado(idEmpleado);

        boolean updateEmpleado = this.empleadoService.updateEmpleado(empleado);

        return new ResponseEntity<>(
                updateEmpleado ? "Empleado actualizado" : "No se ha podido actualizar el empleado",
                updateEmpleado ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //Borrar empleado
    @DeleteMapping("/empleados/{idEmpleado}")
    public ResponseEntity<String> deleteEmpleado(@PathVariable String idEmpleado) {

        System.out.println("Delete en Controller");
        System.out.println("El id empleado del empleado a borrar es " + idEmpleado);

        if (idEmpleado == null) {
            return new ResponseEntity<>("No se ha podido borrar el empleado porque el ID debe ser obligatorio.", HttpStatus.NOT_ACCEPTABLE);
        } else {

            boolean deleteEmpleado = this.empleadoService.deleteEmpleado(idEmpleado);

            return new ResponseEntity<>(
                    deleteEmpleado ? "Empleado borrado" : "Empleado no encontrado",
                    deleteEmpleado ? HttpStatus.OK : HttpStatus.NOT_FOUND
            );
        }
    }

    public ResponseEntity<String> validarEmpleado(EmpleadoModel empleado){

        if(empleado.getNombre() == null) {
            return new ResponseEntity<>(ERROR_CREATE+"el NOMBRE "+NO_INFORMADO,HttpStatus.NOT_ACCEPTABLE);
        }

        if(empleado.getApellido() == null) {
            return new ResponseEntity<>(ERROR_CREATE+"el APELLIDO "+NO_INFORMADO,HttpStatus.NOT_ACCEPTABLE);
        }

        if(empleado.getEdad()<=0) {
            return new ResponseEntity<>(ERROR_CREATE+"la EDAD "+VALOR_ERRONEO,HttpStatus.NOT_ACCEPTABLE);
        }

        if(empleado.getCategoria() == null) {
            return new ResponseEntity<>(ERROR_CREATE+"la CATEGORIA "+NO_INFORMADO,HttpStatus.NOT_ACCEPTABLE);
        }

        if(empleado.getEmpresa() == null) {
            return new ResponseEntity<>(ERROR_CREATE+"la EMPRESA "+NO_INFORMADO,HttpStatus.NOT_ACCEPTABLE);
        }

        return null;

    }

}