import { LitElement, html } from "lit-element";

class EmpleadoFichaListado extends LitElement{

    static get properties(){
        return{
            idEmpleado: {type: Number},
            nombre: {type: String},
            apellido: {type: String},
            edad: {type: Number},
            categoria: {type: String},
            empresa: {type: String}
        };
    }

    constructor(){
        super();
        console.log("Constructor empleado-ficha-listado");
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div class="card-body">
                <h5 class="card-title"> Id Empleado: ${this.idEmpleado}</h5>
                <p class="card-text"> Nombre: ${this.nombre}</p>
                <p class="card-text"> Apellido: ${this.apellido}</p>
                <p class="card-text"> Edad: ${this.edad}</p>
                <p class="card-text"> Categoría: ${this.categoria}</p>
                <p class="card-text"> Empresa: ${this.empresa}</p>
            </div>
            <div class="card-footer">
                <button @click="${this.deleteEmpleado}" class="btn btn-danger col-5"><strong>Borrar</strong></button>
                <button @click="${this.updateEmpleado}" class="btn btn-info col-5 offset-1"><strong>Actualizar</strong></button>            
            </div>
        `;
    }

    deleteEmpleado (e){
        console.log("deleteEmpleado en empleado-ficha-listado");
        console.log("Se va a borrar la persona de id: " + this.idEmpleado)

        //evento no nativo        
        this.dispatchEvent(
            new CustomEvent(
                "delete-empleado", {
                    "detail" : {
                        "idEmpleado": this.idEmpleado
                    }
                }
            )
        )
    }

    updateEmpleado (e){
        console.log("updateEmpleado en empleado-ficha-listado");
        console.log("Se va a modificar más información de la persona de id: " + this.idEmpleado)

        //evento no nativo        
        this.dispatchEvent(
            new CustomEvent(
                "update-empleado", {  
                    "detail" : {
                        "idEmpleado": this.idEmpleado
                    }
                }
            )
        )        
    }        

}

customElements.define('empleado-ficha-listado', EmpleadoFichaListado);