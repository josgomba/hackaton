import { LitElement, html } from "lit-element";

class EmpleadoHeader extends LitElement{

    static get properties(){
        return{
        };
    }

    constructor(){
        super();
    }

    render(){
        return html`
            <h1>Aplicación Empleados</h1>
        `;
    }
}

customElements.define('empleado-header', EmpleadoHeader);