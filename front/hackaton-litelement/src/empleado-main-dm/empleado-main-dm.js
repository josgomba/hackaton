import { LitElement, html } from 'lit-element';


class EmpleadoMainDm extends LitElement {

    static get properties() {
        return {
            empleados: {type: Array},
            empleado: {type: Object}
        };
    }

    constructor() {
        super();
        this.empleados = [];
        this.empleado = null;
        this.getEmpleadoData();
    }

    render() {
        return html`
        `;
    }


    getEmpleadoData(){
        console.log("getEmpleadosDM");
        console.log("obteniendo datos de los empleados");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
                if(xhr.status === 200) {
                    console.log("200");
                    let APIResponse = JSON.parse(xhr.response);
                    this.empleados = APIResponse;
                }
        }


        xhr.open("GET", "http://localhost:8080/hackaton/v1/empleados");
        xhr.send();

        console.log("fin empleado get data")

    }

    actualizarEmpleadoData(){
        console.log("putEmpleadosDM");

        let xhr = new XMLHttpRequest();

        xhr.open("PUT", "https://swapi.dev/api/films/");
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(this.empleados));
        console.log("fin empleado put data")

    }

    crearEmpleadoData(){
        console.log("crearEmpleadosDM");

        let xhr = new XMLHttpRequest();

        //xhr.open("POST", "https://swapi.dev/api/films/");
        xhr.open("http://localhost:8080/hackaton/v1/empleados");
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(this.empleado));
        console.log("fin empleado crear data")

    }


    borrarEmpleadoData(){
        console.log("borrarcrearEmpleadosDM");

        let xhr = new XMLHttpRequest();

        xhr.open("DELETE", "https://swapi.dev/api/films/");
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send();

        console.log("fin empleado borrar data")

    }

    updated(changedProperties) {
        console.log("updated recogemos empleado dm");

        if (changedProperties.has("empleados")) {
            this.dispatchEvent(
                new CustomEvent(
                    "empleado-data-list",
                    {
                        detail : {
                            empleados: this.empleados
                        }
                    }
                )
            )

        }

    }
}

customElements.define('empleado-main-dm', EmpleadoMainDm)