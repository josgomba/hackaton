import { LitElement, html } from "lit-element";
import '../empleado-ficha-listado/empleado-ficha-listado'
import '../empleado-main-dm/empleado-main-dm'
import '../empleado-form/empleado-form'

class EmpleadoMain extends LitElement{

    static get properties(){
        return{
            empleados:{type: Array},
            showEmpleadoForm: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.empleados = [];
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Empleados</h2>
            <div class="row" id="empleadosList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.empleados.map(
                    empleado => html  `<empleado-ficha-listado
                                        idEmpleado="${empleado.idEmpleado}"
                                        nombre="${empleado.nombre}"
                                        apellido="${empleado.apellido}"
                                        edad="${empleado.edad}"
                                        categoria="${empleado.categoria}"
                                        empresa="${empleado.empresa}"
                                        @delete-empleado="${this.deleteEmpleado}"
                                        @update-empleado="${this.updateEmpleado}">
                                    </empleado-ficha-listado>`
                )}
                </div>
            </div>
            <empleado-main-dm
                @empleado-data-list="${this.empleadoDataList}">
            </empleado-main-dm>
            <div class="row">
                <empleado-form
                    @empleado-form-close="${this.empleadoFormClose}" 
                    @empleado-form-store="${this.empleadoFormStore}"
                    class="d-none border rounded border-primary" 
                    id="empleadoForm"
                    >
                </empleado-form>
            </div>

        `;
    }

    empleadoDataList(e){
        console.log("empleadoDataList");
        this.empleados = e.detail.empleados;
    }

    deleteEmpleado(e){
        console.log("deleteEmpleado en empleado-main");
        console.log("Se va a borrar la persona con id " + e.detail.idEmpleado);

        //this.empleados = this.empleados.filter(
         //   empleado => empleado.idEmpleado != e.detail.idEmpleado
        //);

        let xhr = new XMLHttpRequest();

        xhr.open("DELETE","http://localhost:8080/hackaton/v1/empleados/"
         + e.detail.idEmpleado);
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        console.log("fin empleado deleete data")
        console.log("Persona borrada");

        xhr.send();

        console.log("actualizamos la lista con otro get")
        let xhrUpd = new XMLHttpRequest();

        xhrUpd.onload = () => {
                if(xhrUpd.status === 200) {
                    console.log("200");
                    let APIResponse = JSON.parse(xhrUpd.response);
                    this.empleados = APIResponse;
                }
        }

        xhrUpd.open("GET", "http://localhost:8080/hackaton/v1/empleados");
        xhrUpd.send();

    }

    updateEmpleado(e){
        console.log("updateEmpleado en empleado-main");
        console.log("Se va a actualizar la persona con id " + e.detail.idEmpleado);


        let chosenEmpleado = this.empleados.filter(
            empleado => empleado.idEmpleado == e.detail.idEmpleado
        );

        console.log(this.empleados);

        let person  = {};
        person.idEmpleado = chosenEmpleado[0].idEmpleado;
        person.nombre = chosenEmpleado[0].nombre;
        person.apellido = chosenEmpleado[0].apellido;
        person.edad = chosenEmpleado[0].edad;
        person.categoria = chosenEmpleado[0].categoria;
        person.empresa = chosenEmpleado[0].empresa;

        console.log("chosen " + chosenEmpleado[0].idEmpleado);

        this.shadowRoot.getElementById("empleadoForm").empleado = person;
        this.shadowRoot.getElementById("empleadoForm").editingEmpleado = true;
        this.showEmpleadoForm = true;
    }

    updated(changedProperties) {
        console.log("updated");

        if (changedProperties.has("showEmpleadoForm")) {
            console.log("Ha cambiado el valor de la propiedad showEmpleadoForm en persona-main");

            if (this.showEmpleadoForm === true) {
                this.showEmpleadoFormData();
            } else {
                this.showEmpleadoList();
            }
        }
    }

    empleadoFormClose() {
        console.log("empleadoFormClose");
        console.log("Se ha cerrado el formulario del empleado");
        this.showEmpleadoForm = false;
    }

    empleadoFormStore(e) {
        console.log("empleadoFormStore");
        console.log("Se va a almacenar un empleado");
        console.log(e.detail.empleado);

     /*   if (e.detail.editingEmpleado === true){
            console.log("Se va a actualizar la Persona de  nombre " + e.detail.empleado.nombre);    
            this.empleados = this.empleados.map(
                person => person.idEmpleado === e.detail.empleado.idEmpleado
                    ? person = e.detail.empleado : person);
        } else{
                console.log("Se va a almacenar una persona nueva");    
                this.empleados = [...this.empleados, e.detail.empleado];
        } */

        //this.empleados.push(e.detail.empleado);

        console.log("crearEmpleadosDM");

        let xhr = new XMLHttpRequest();

        if (e.detail.editingEmpleado === true){

            xhr.open("PUT","http://localhost:8080/hackaton/v1/empleados/"
             + e.detail.empleado.idEmpleado);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.send(JSON.stringify(e.detail.empleado));

            console.log("fin empleado update data")
            console.log("Persona actualizada");

        } else {

        //xhr.open("POST", "https://swapi.dev/api/films/");
        xhr.open("POST","http://localhost:8080/hackaton/v1/empleados");
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(JSON.stringify(e.detail.empleado));

        console.log("fin empleado crear data")
        console.log("Persona almacenada");

        }
        console.log("actualizamos la lista con otro get")
        let xhrUpd = new XMLHttpRequest();

        xhrUpd.onload = () => {
                if(xhrUpd.status === 200) {
                    console.log("200");
                    let APIResponse = JSON.parse(xhrUpd.response);
                    this.empleados = APIResponse;
                }
        }


        xhrUpd.open("GET", "http://localhost:8080/hackaton/v1/empleados");
        xhrUpd.send();

        this.showEmpleadoForm = false;
    }

    showEmpleadoFormData() {
        console.log("showEmpleadoFormData");
        console.log("Mostrando formulario de empleado");
        this.shadowRoot.getElementById("empleadoForm").classList.remove("d-none");
        this.shadowRoot.getElementById("empleadosList").classList.add("d-none");
    }

    showEmpleadoList() {
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("empleadoForm").classList.add("d-none");
        this.shadowRoot.getElementById("empleadosList").classList.remove("d-none");
    }


}

customElements.define('empleado-main', EmpleadoMain);