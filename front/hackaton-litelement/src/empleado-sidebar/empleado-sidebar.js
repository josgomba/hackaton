import { LitElement, html } from 'lit-element';

class EmpleadoSidebar extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    newEmpleado(e) {
        console.log("newEmpleado");
        console.log("Se va a crear un nuevo empleado");

        this.dispatchEvent(new CustomEvent("new-empleado", {}));
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
			<aside>
				<section>
					<div class="mt-5">
						<button class="w-100 btn bg-success" style="font-size: 50px" @click="${this.newEmpleado}"><strong>+</strong></button>
                    </div>
				</section>
			</aside>
		`;
    }
}

customElements.define('empleado-sidebar', EmpleadoSidebar)