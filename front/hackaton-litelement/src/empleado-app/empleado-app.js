import { LitElement, html } from "lit-element";
import '../empleado-header/empleado-header'
import '../empleado-main/empleado-main'
import '../empleado-footer/empleado-footer'
import '../empleado-sidebar/empleado-sidebar'

class EmpleadoAPP extends LitElement{

    static get properties(){
        return{
        };
    }

    constructor(){
        super();
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <empleado-header></empleado-header>
            <div class="row">
                <empleado-sidebar @new-empleado="${this.newEmpleado}" class="col-2"></empleado-sidebar>
                <empleado-main class="col-10"></empleado-main>
            </div>
            <empleado-footer></empleado-footer>
        `;
    }

    newEmpleado(e) {
		console.log("newEmpleado en empleadoApp");	
		this.shadowRoot.querySelector("empleado-main").showEmpleadoForm = true; 	  	
	}
}

customElements.define('empleado-app', EmpleadoAPP);