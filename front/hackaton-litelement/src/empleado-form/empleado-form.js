import { LitElement, html } from 'lit-element';

class EmpleadoForm extends LitElement {

    static get properties() {
        return {
            empleado: {type: Object},
            editingEmpleado: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.empleado = {};

        this.resetFormData();
    }


    updateNombre(e) {
        console.log("updateNombre");
        console.log("Actualizando la propiedad nombre con el valor " + e.target.value);

        this.empleado.nombre = e.target.value;
    }

    updateApellido(e) {
        console.log("updateApellido");
        console.log("Actualizando la propiedad apellido con el valor " + e.target.value);

        this.empleado.apellido = e.target.value;
    }
    updateCategoria(e) {
        console.log("updateCategoria");
        console.log("Actualizando la propiedad categoria con el valor " + e.target.value);

        this.empleado.categoria = e.target.value;
    }

    updateEdad(e) {
        console.log("update edad");
        console.log("Actualizando la propiedad categoria con el valor " + e.target.value);

        this.empleado.edad = e.target.value;
    }

    updateEmpresa(e) {
        console.log("updateEmpresa");
        console.log("Actualizando la propiedad empresa con el valor " + e.target.value);

        this.empleado.empresa = e.target.value;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();

        this.resetFormData();

        this.dispatchEvent(new CustomEvent("empleado-form-close", {}));
    }

    storeempleado(e) {
        console.log("storeempleado");
        e.preventDefault();

        console.log("La propiedad nombre vale " + this.empleado.nombre);
        console.log("La propiedad apellido vale " + this.empleado.apellido);
        console.log("La propiedad edad vale " + this.empleado.edad);

        this.dispatchEvent(
            new CustomEvent(
                "empleado-form-store",
                {
                    detail: {
                        empleado: {
                            idEmpleado: this.empleado.idEmpleado,
                            nombre: this.empleado.nombre,
                            apellido: this.empleado.apellido,
                            categoria: this.empleado.categoria,
                            empresa: this.empleado.empresa,
                            edad: this.empleado.edad
                        },
                        editingEmpleado: this.editingEmpleado
                    }
                }
            )
        )

        this.resetFormData();
    }

    render() {
        return html`
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
                <form>
                    </div>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input @input="${this.updateNombre}" type="text" class="form-control" placeholder="Nombre completo" 
                        .value="${this.empleado.nombre}"
                        ?disabled="${this.editingEmpleado}"/>
                    </div>
                    <div class="form-group">
                        <label>Apellidos</label>
                        <textarea @input="${this.updateApellido}" class="form-control" placeholder="Apellidos" rows="5"
                        .value="${this.empleado.apellido}"
                        ?disabled="${this.editingEmpleado}">
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label>Edad</label>
                        <input @input="${this.updateEdad}" type="text" class="form-control" placeholder="Edad" 
                        .value="${this.empleado.edad}"/>
                    </div>
                    <div class="form-group">
                        <label>Categoria</label>
                        <input @input="${this.updateCategoria}" type="text" class="form-control" placeholder="Categoria" 
                        .value="${this.empleado.categoria}"/>
                    </div>
                    <div class="form-group">
                        <label>Empresa</label>
                        <input @input="${this.updateEmpresa}" type="text" class="form-control" placeholder="Empresa" 
                        .value="${this.empleado.empresa}"/>
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storeempleado}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
             </div>
        `;
    }

    resetFormData() {
        console.log("resetFormData");

        this.empleado = {};
        this.empleado.nombre = "";
        this.empleado.apellido = "";
        this.empleado.categoria = "";
        this.empleado.empresa = "";
        this.empleado.edad = "";

        this.editingEmpleado = false;
    }    
}

customElements.define('empleado-form', EmpleadoForm)